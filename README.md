# ELG conversion tools

This software was initially developed with the aim to transform (ELRA) META-SHARE v3.1 metadata documents to ELG-SHARE v1.0.2.

## Schemas

### `META-SHARE`

Input resources MUST be described using the metadata schema developed for [METASHARE](http://www.meta-net.eu/meta-share) v3.1. The META-SHARE metadata schema  aims at the  description of Language Resources (corpora, lexical/conceptual resources, language descriptions  and technologies). The relevant documentation of the model is available at [http://www.meta-net.eu/meta-share](http://www.meta-net.eu/meta-share/META-SHARE%20%20documentationUserManual.pdf/at_download/file). 

### `ELG-SHARE`

Transformed resources will be described using the metadata schema [ELG-SHARE]() v1.0.2. The ELG-SHARE model is the backbone of the [ELG Platform](https://www.european-language-grid.eu) and covers both functional (Tools/Service) and non-functional (Corpus, LanguageDescription, Lexical/Conceptual) LT-Resources as well as entities such persons, organizations, projects, documents and licences. The relevant documentation of the model is available at [https://www.european-language-grid.eu/metadata](https://www.european-language-grid.eu/metadata). 

## Dependencies

- **`Saxon-B`** (version >= 9.1.0)  An open-source XQuery processor. Under Linux, install using ``sudo apt-get install libsaxonb-java``
- **`xmllint`**. (version >= 20904)  A checker and transformer tool for XML files, distributed with the libxml package. Under Linux, install using ``sudo apt-get install libxml2-utils``
- **`Bash`** (version >= 4.2.0) Type `bash --version` to check your installed version. Under Linux, to upgrade Bash type `apt-get update` and then `apt-get install --only-upgrade bash`

## Getting Started

Start cloning the repository:

    git clone https://gitlab.com/cogniteva/elg-conversion-tools
    cd elg-conversion-tools

Then, if necessary, set the following environment variables:

| Environment variable     | Description                                                                        | Default     |
| ------------------------ | ---------------------------------------------------------------------------------- | ----------- |
| `MINIMIZE_ELG_XML`       | `export MINIMIZE_ELG_XML=0` to disable removing blanks in the output file          | `1`         |
| `OUTPUT_ELG_XML_FOLDER`  | `export OUTPUT_ELG_XML_FOLDER=/foo/bar` to change the default output directory     | `./elg-xml` |

## Usage

    Usage:
        convert-to-elg.sh XML_FILE [ELG_SHARE_XSD]

## Examples

| Command                                                                             | Description                                                                           |
| ----------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| `./convert-to-elg.sh foo.xml`                                                       | Transforms `foo.xml` to `foo-elg.xml`, by default results are stored in `./elg-xml` |
| `./convert-to-elg.sh foo.xml ELG-SHARE.xsd`                                         | Transforms `foo.xml` and validates the result using `ELG-SHARE.xsd`                   |
| `ls -1 metashare/*.xml \| xargs -n1 -I '{}' ./convert-to-elg.sh '{}'`               | Transforms all XML files under the `metashare` directory                              |
| `ls -1 metashare/*.xml \| xargs -n1 -I '{}' ./convert-to-elg.sh '{}' ELG-SHARE.xsd` | Transforms all XML files and validates each result using `ELG-SHARE.xsd`              |
| `./xsd-bulk-validator.sh ELG-SHARE.xsd ./elg-xml`                                   | Validates all XML files under the `./elg-xml` directory  using `ELG-SHARE.xsd`        |

## Contributing

> Bash code is ShellCheck-compliant. See http://www.shellcheck.net/about.html for information about how to run ShellCheck locally.

You are welcome to contribute to improve this software. Below are some of the things that you can do to contribute:

-  [Fork](https://gitlab.com/cogniteva/elg-conversion-tools/-/forks/new) and [request a pull](https://gitlab.com/cogniteva/elg-conversion-tools/-/merge_requests) to the [master branch](https://gitlab.com/cogniteva/elg-conversion-tools/-/tree/master).
-  Submit [bug reports or feature requests](https://gitlab.com/cogniteva/elg-conversion-tools/-/issues/new)

## License

This software is licensed under the [CC-BY-NC-SA 4.0](/LICENSE). Contact licensing@cogniteva.fr for further inquiries.

## Disclaimer

This software was developed by [Cogniteva] and is provided without any WARRANTY (EXPRESS or IMPLIED) and is intended to provide in good faith to the community a means of transforming META-SHARE metadata into ELG-SHARE documents.

Certain commercial and non-commercial firms and trade names are identified in order to deal with specific transformations. Such use is not intended to imply any affiliation with, sponsorship, or endorsement of this software.

## Acknowledgements

The authors would like to thank Victoria Arranz and Valérie Mapelli from [ELDA](http://elda.org) who, under the umbrella of the [ELG initiative](https://www.european-language-grid.eu), provided valuable suggestions and corrections for the first version of this software.

The authors would also like to thank Penny Labropoulou from the [ILSP](http://www.ilsp.gr) who, under the umbrella of the [ELG initiative](https://www.european-language-grid.eu), provided initial information on the correspondence between the META-SHARE and ELG-SHARE documents.

---
Copyright (C) 2020 [Cogniteva]

[cogniteva]: https://cogniteva.com
